# mac_plist_ripper  #

This is currently in an experimental (alpha) state.  There are bugs and incomplete features. 

##   What is it?

  mac_plist_ripper.py is a Python script that will scan a supplied folder,
  search for plists (by file signature) and process them using a plugin
  (if there is one available for the particular plist). It uses code from CCL
  https://code.google.com/p/ccl-bplist/.


##  Installation

This code has been tested with Python 3.3.3 on Mac OS X 10.9.1

    git clone https://bitbucket.org/chrishargreaves/mac_plist_ripper.git
    cd mac_plist_ripper/src
    git clone https://code.google.com/p/ccl-bplist/
    mv ccl-bplist cclbplist

Make sure you have Python 3 and run mac_plist_ripper.py

## Licensing


  Please see the file called LICENSE.


## Contacts

o Code is hosted at https://bitbucket.org/chrishargreaves/mac_plist_ripper

o Alternatively contact Chris Hargreaves at christopherhargreavesuk@gmail.com



## Usage instructions for the program.



./mac_plist_ripper.py --help

    usage: mac_plist_ripper.py [-h] [-a] [-r] [-f] [-l] [-b {raw,hide,strings}]
                             [-d DUMPDIR] [-i INCLUDE [INCLUDE ...]]
                             [-x EXCLUDE [EXCLUDE ...]]
                             path

    Locate and parse plists

    positional arguments:
      path                  path to search for plists - can be a mounted disk
                            image

    optional arguments:
      -h, --help            show this help message and exit
      -a, --all             display report for all plists, even if no plugin is
                            available
      -r, --recursive       examine all subfolders in supplied path
      -f, --filter          run in filter mode; full raw contents of plists will
                            be displayed, but only for those with a plugin
      -l, --list            list plists, do not process
      -b {raw,hide,strings}, --bytes {raw,hide,strings}
                            choose how to display non-printable data
      -d DUMPDIR, --dumpdir DUMPDIR
                            choose output directory for reports rather than
                            printing
      -i INCLUDE [INCLUDE ...], --include INCLUDE [INCLUDE ...]
                            include files only with specified extensions (takes
                            precedence over exclude)
      -x EXCLUDE [EXCLUDE ...], --exclude EXCLUDE [EXCLUDE ...]
                            exclude files with specified extensions



Example Usage

Listing of all binary plists showing only those that have a .plist extension:

        ./mac_plist_ripper.py /Users/chris -r -l --include plist

Listing of all binary plists showing only those that do not have a .nib extension:

        ./mac_plist_ripper.py /Users/chris/ -r -l --exclude nib

Search recursively in /Users/chris. Only include files with .plist extension. Process only those that have plugins available

        ./mac_plist_ripper.py /Users/chris -r --include plist


Search recursively in /Users/chris. Include files only with .plist. Also run against those without plugins. Dump all output to specified directory

        ./mac_plist_ripper.py /Users/chris -r --all --include plist --dumpdir /Users/chris/Desktop/plists_reports/

Run against a specific plist (if a plugin is avaliable)

        ./mac_plist_ripper.py /Users/chris/Library/Preferences/com.apple.TextEdit.LSSharedFileList.plist

Run against a specific plist and get raw output (even if no plugin)

        ./mac_plist_ripper.py /Users/chris/Library/Preferences/com.apple.sidebarlists.plist -f -a

Run against all plists, instead of suppressing binary data values, hide non-printable, but display strings

        ./mac_plist_ripper.py /Users/chris/Library/Preferences -r --include plist --bytes strings
import logging
import pprint
import cclbplist.ccl_bplist
from core import reporter
import core.plist_finder

class PlistProcessor(object):

    def __init__(self):
        self.matching_criteria = {}
        self.field_list = {}
        self.report = ""

    def __open_binary_plist(self, file_pointer):
        self.report = ""
        file_pointer.seek(0)
        self.__plist_parser_data = cclbplist.ccl_bplist.load(file_pointer)

    def __open_xml_plist(self, file_pointer):
        # TODO
        self.__plist_parser_data = {}

    def __open_plist(self, path):
        file_pointer = open(path, 'rb')
        header = file_pointer.read(512)
        if core.plist_finder.is_file_binary_plist(header):
            self.__open_binary_plist(file_pointer)
        elif core.plist_finder.is_file_xml_plist(header):
            self.__open_xml_plist(file_pointer)
            self.report = "XML Plist (not processed yet)"
        else:
            raise TypeError('File {} not a plist'.format(path))

    def get_report(self, path):
        self.report = ""
        try:
            self.__open_plist(path)
        except Exception as e:
            logging.error("An Exception occurred ({}) processing {}".format(e, path))
            return "ERROR"

        for each_field in self.field_list:
            key_name = self.field_list[each_field]
            search_results = []
            self.search_dict_for_key(self.__plist_parser_data, key_name, search_results)
            self.report += reporter.get_report(each_field, key_name, search_results)

        if self.report == "":
            self.report = " - Plugin matched but failed to generate any report data"

        return self.report

    def search_dict_for_key(self, the_dict, the_key, results):
        """Recursively search for specified key name"""
        if isinstance(the_dict, dict):
            for each_key in the_dict:
                if each_key == the_key:
                    results.append(the_dict[each_key])
                else:
                    self.search_dict_for_key(the_dict[each_key], the_key, results)
        elif isinstance(the_dict, list):
            for each_value in the_dict:
                self.search_dict_for_key(each_value, the_key, results)




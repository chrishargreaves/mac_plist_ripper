__author__ = 'Chris Hargreaves'


import logging
import pprint
import core.plist_finder
import core.base_plugin
import cclbplist.ccl_bplist

class PlistProcessor(core.base_plugin.PlistProcessor):
    """This plugin can be used for plists that do not have specific parsers. It will produce a report that
    displays all the key value pairs. """

    def __open_plist(self, path):
        file_pointer = open(path, 'rb')
        header = file_pointer.read(512)
        if core.plist_finder.is_file_binary_plist(header):
            self.__open_binary_plist(file_pointer)
            self.report = pprint.pformat(self.__plist_parser_data)
        elif core.plist_finder.is_file_xml_plist(header):
            self.__open_xml_plist(file_pointer)
            self.report = "XML Plist (not processed yet)"
        else:
            raise TypeError('File {} not a plist'.format(path))

    def get_report(self, path):
        try:
            self.__open_plist(path)
        except UnicodeDecodeError:
            msg = "Unicode error reading {}".format(path)
            logging.error(msg)
            print(msg)
            return "ERROR"
        except Exception as e:
            msg = "Unknown exception ({}) handling {}".format(str(e), path)
            logging.error(msg)
            print(msg)
            return "ERROR"

        return self.report

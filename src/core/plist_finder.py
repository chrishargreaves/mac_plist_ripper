__author__ = 'Chris Hargreaves'

import os
import logging
import re

def get_list_of_plists(target_directory, include_extensions=(), exclude_extensions=()):
    """Returns a list of binary plists in a directory"""
    list_of_files = []
    for each in os.listdir(target_directory):
        full_path = os.path.join(target_directory, each)
        if is_file_to_be_included_in_list(full_path, include_extensions, exclude_extensions):
            list_of_files.append(full_path)
    return list_of_files


def get_recursive_list_of_plists(target_directory, include_extensions=(), exclude_extensions=()):
    """Returns a list of all plists in this folder and any subfolders"""
    list_of_files = []
    results = os.walk(target_directory)
    for root, dirs, files in results:
        for each_file in files:
            full_path = os.path.join(root , each_file)
            try:
                if is_file_to_be_included_in_list(full_path, include_extensions, exclude_extensions):
                    list_of_files.append(full_path)
                else:
                    pass
            except IOError:
                logging.error("IOError reading {}".format(full_path))

    return list_of_files


def is_file_to_be_included_in_list(full_path, include_extensions=(), exclude_extensions=()):

    if include_extensions is None:
        include_extensions = ()
    if exclude_extensions is None:
        exclude_extensions = ()

    if os.path.isfile(full_path):
        if not os.path.islink(full_path):
            this_extension = os.path.splitext(full_path)[-1].strip('.')

            if len(include_extensions) > 0:  # include takes priority over exclude
                if this_extension in include_extensions:
                    if is_file_plist(full_path):
                        return True
            else:
                if this_extension not in exclude_extensions:
                    if is_file_plist(full_path):
                        return True
    return False


def is_file_plist(full_path_to_file):
    """Returns if a file has a 'bplist' header or not"""
    if not os.path.exists(full_path_to_file):
        return False
    if not os.path.isfile(full_path_to_file):
        return False

    try:
        # open file and check header
        f = open(full_path_to_file, "rb")
        header = f.read(512)
        if is_file_binary_plist(header):
            return True
        if is_file_xml_plist(header):
            #logging.debug("Found XML plist in {}".format(full_path_to_file))
            return True
    except IOError:
        logging.error("Error reading {}".format(full_path_to_file))

    return False


def is_file_binary_plist(data):
    """Returns True if data has a Binary Plist Header"""
    if data[0:6] == b"bplist":
        return True
    else:
        return False


def is_file_xml_plist(data):
    """Returns True if data has an XML Plist Header"""
    if re.search(b'<\?xml version=', data) and re.search(b"<plist version=", data):
        return True
    else:
        return False


def get_bytes_as_hex_string(data):
    out_str = ""
    for each_byte in data:
        out_str += "".join('{:02X} '.format(each_byte))
    return out_str.strip()

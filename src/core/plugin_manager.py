from core import default

__author__ = 'Chris Hargreaves'

import re
import os
import logging

import plugins.global_preferences_user
import plugins.finder
import plugins.partial_downloads
import plugins.acrobat_pro
import plugins.illustrator
import plugins.indesign
import plugins.lightroom3
import plugins.photoshop
import plugins.airport_utility
import plugins.dock
import plugins.facetime
import plugins.game_centre_daemon
import plugins.ichat_jabber
import plugins.iphoto
import plugins.net_auth_agent
import plugins.preview
import plugins.quicktime
import plugins.quicktime_sandboxed
import plugins.safari_settings
import plugins.screen_sharing
import plugins.sidebars
import plugins.app_store
import plugins.text_edit
import plugins.byword
import plugins.skype
import plugins.mobile_me
import plugins.printing_prefs
import plugins.vlc
import plugins.app_store_updates
import plugins.maps
import plugins.systemconfigurationpreferences
import plugins.loginwindow
import plugins.software_updates
import plugins.systemconfigurationpreferences
import plugins.global_preferences_system
import plugins.loginitems
import plugins.system_version
import plugins.var_db_receipts
import plugins.safari_bookmarks
import plugins.safari_topsites

class PluginManager():

    def __init__(self, display_unknown=False, filter_mode=False, handle_bytes='hide'):
        """test"""
        self.display_unknown = display_unknown
        self.filter_mode = filter_mode
        self.handle_bytes = handle_bytes
        self.__setup_plugins()


    def identify_root_path(self, path):

        test_path = path
        root_path = None
        while True:
            if os.path.ismount(test_path):
                root_path = test_path

            test_path = os.path.dirname(test_path)

            if test_path == "/":
                break

        return root_path


    def __setup_plugins(self):
        """Detects all available plugins"""
        self.list_of_plugins = []
        self.list_of_plugins.append(plugins.global_preferences_user.GlobalPreferencesParserUser())
        self.list_of_plugins.append(plugins.finder.FinderParser())
        self.list_of_plugins.append(plugins.partial_downloads.PartialDownloadParser())
        self.list_of_plugins.append(plugins.acrobat_pro.AcrobatProParser())
        self.list_of_plugins.append(plugins.illustrator.IllustratorParser())
        self.list_of_plugins.append(plugins.indesign.InDesignParser())
        self.list_of_plugins.append(plugins.lightroom3.Lightroom3Parser())
        self.list_of_plugins.append(plugins.photoshop.PhotoshopParser())
        self.list_of_plugins.append(plugins.airport_utility.AirportUtilityParser())
        self.list_of_plugins.append(plugins.dock.DockParser()) # doesn't work as recursive search needs fixing
        self.list_of_plugins.append(plugins.facetime.FacetimeParser())
        self.list_of_plugins.append(plugins.game_centre_daemon.GamedParser())
        self.list_of_plugins.append(plugins.ichat_jabber.iChatJabberParser())
        self.list_of_plugins.append(plugins.iphoto.iPhotoParser())
        self.list_of_plugins.append(plugins.net_auth_agent.NetAuthAgentParser())
        self.list_of_plugins.append(plugins.preview.PreviewParser()) # doesn't work as recursive search needs fixing
        self.list_of_plugins.append(plugins.quicktime.QuicktimeParser()) # doesn't work as recursive search needs fixing
        self.list_of_plugins.append(plugins.quicktime_sandboxed.QuicktimeParser()) # doesn't work as recursive search needs fixing
        # need to do com.apple.recentitems.plist - Recent Applications, Documents and Servers
        self.list_of_plugins.append(plugins.safari_settings.SafariSettingsParser())
        self.list_of_plugins.append(plugins.screen_sharing.ScreenSharingParser())
        self.list_of_plugins.append(plugins.sidebars.SidebarListsParser())
        self.list_of_plugins.append(plugins.app_store.AppStoreParser())
        self.list_of_plugins.append(plugins.text_edit.TextEditParser())
        self.list_of_plugins.append(plugins.byword.BywordParser())
        self.list_of_plugins.append(plugins.skype.SkypeParser())
        self.list_of_plugins.append(plugins.mobile_me.MobileMeSettingsParser())
        self.list_of_plugins.append(plugins.printing_prefs.PrintingPrefsParser())
        self.list_of_plugins.append(plugins.vlc.VlcParser())
        self.list_of_plugins.append(plugins.app_store_updates.AppStoreUpdatesParser())
        self.list_of_plugins.append(plugins.maps.MapsParser())
        self.list_of_plugins.append(plugins.systemconfigurationpreferences.SystemConfigurationPreferencesParser())
        self.list_of_plugins.append(plugins.loginwindow.LoginWindowParser())
        self.list_of_plugins.append(plugins.software_updates.SoftwareUpdateParser())
        self.list_of_plugins.append(plugins.systemconfigurationpreferences.SystemConfigurationPreferencesParser())
        self.list_of_plugins.append(plugins.global_preferences_system.GlobalPreferencesParserSystem())
        self.list_of_plugins.append(plugins.loginitems.LoginItemsParser())
        self.list_of_plugins.append(plugins.system_version.SystemVersionParser())
        self.list_of_plugins.append(plugins.var_db_receipts.VarDbReceiptParser())
        self.list_of_plugins.append(plugins.safari_bookmarks.SafariBookmarkParser())
        self.list_of_plugins.append(plugins.safari_topsites.SafariTopSitesParser())

    def does_target_plist_match_any_plugins(self, path_to_plist):
        """Checks if the supplied plist matches any of the plugins available"""
        for each_plugin in self.list_of_plugins:
            if re.search(each_plugin.matching_criteria['path'], path_to_plist):
                return True
        return False


    def handle_plist(self, path_to_plist):
        """Takes a path to a plist and returns a report on it as a string. Returns None if no plugin available
         and display_unknown is set to False"""
        handled = False
        report = None

        root_path = self.identify_root_path(path_to_plist)
        path_to_check = re.sub("^{}".format(root_path), "", path_to_plist)

        for each_plugin in self.list_of_plugins:
            if re.search(each_plugin.matching_criteria['path'], path_to_check):
                if self.filter_mode is True:
                    # if in filter mode, don't run the plugin, just use to decide which to process
                    report_from_parser = self.generate_raw_report(path_to_plist)
                else:
                    report_from_parser = each_plugin.get_report(path_to_plist)

                report = '=' * 80 + '\n'
                report += ("Plugin '{}' matched {}\n".format(each_plugin.title, path_to_plist))
                report += '=' * 80 + '\n'

                if self.handle_bytes == 'hide':
                    report += re.sub(r"""b['"].+["']""", '[bytes removed]', report_from_parser)
                elif self.handle_bytes == 'strings':
                    # replaces any runs of 1 or more non-printable hex characters with '...'
                    report += re.sub(r'''(?:\\x[0-9a-z]{2})+''', '...', report_from_parser)
                elif self.handle_bytes == 'raw':
                    report += report_from_parser
                else:
                    raise ValueError('Unknown value supplied for byte display mode ({})'.format(self.handle_bytes))

                handled = True
                break

        # if gets to end and not processed by any plugins, if it is set to display all
        # then generate a raw report
        if self.display_unknown:
            if handled is False:
                report = self.generate_raw_report(path_to_plist)
        return report


    def generate_raw_report(self, path_to_plist):
        report = '=' * 80 + '\n'
        report += "Raw content of Plist: {}\n".format(path_to_plist)
        report += '=' * 80 + '\n'
        a = default.PlistProcessor()
        report += a.get_report(path_to_plist)
        return report

import pprint

def get_report(each_field, key_name, search_results):
    """Returns a formatted result """
    report = ""
    for each_result in search_results:
        if each_field != key_name:
            report += "{}({}) = {}\n\n".format(each_field, key_name, pprint.pformat(each_result, width=120))
        else:
            report += "{} = {}\n\n".format(each_field, pprint.pformat(each_result, width=120))

    return report


    #
    # def format_results(self, value):
    #
    #     if type(value) == list:
    #         # turn lists into printable lists
    #         res = ''
    #         for each in value:
    #             res = res + '\t\t\t\t\t' + self.format_results(each) + '\n'
    #         return res.lstrip('\t')
    #     if type(value) == dict:
    #         res = self.print_dict(value)
    #         return res
    #     else:
    #         # otherwise just str it
    #         return str(value)
    #
    #
    # def print_dict(self, value):
    #     printable = ""
    #
    #     for each in value:
    #         if type(value[each]) == dict:
    #             printable = printable  + self.print_dict(value[each])
    #         else:
    #             printable = printable + str(value[each])
    #     return printable

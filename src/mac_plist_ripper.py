#! /usr/local/bin/python3

import argparse
import sys
import os
import re
import logging
from core import plist_finder, plugin_manager


def get_list_of_plists(path, recursive_mode, include_extensions=(), exclude_extensions=()):
    """Returns a list of plists in the supplied path. Also lists subdirectories if recursive_mode is True"""
    logging.info('Getting list of plists from {}'.format(path))
    if os.path.isfile(path):
        list_of_plists = [path]
    else:
        if recursive_mode:
            list_of_plists = plist_finder.get_recursive_list_of_plists(path, include_extensions, exclude_extensions)
        else:
            list_of_plists = plist_finder.get_list_of_plists(path, include_extensions, exclude_extensions)
    logging.info('{} plists found'.format(len(list_of_plists)))
    return list_of_plists


def print_list_of_plists(full_list_of_plists, filter_mode=None, plugin_manager=None):
    """Display the list of plists"""
    displayed_count = 0
    for each_plist in full_list_of_plists:
        if not filter_mode:
            print(each_plist)
        else:
            if plugin_manager.does_target_plist_match_any_plugins(each_plist):
                displayed_count += 1
                print(each_plist)
    if not filter_mode:
        print("total {}".format(len(full_list_of_plists)))
    else:
        print("total {}/{}".format(displayed_count, len(full_list_of_plists)))


def analyse_list_of_plists(full_list_of_plists, plugin_manager, dump_dir=None):
    """Analyse the list of plists and print the output"""
    total_plists_processed = 0
    max_filename_len = 255
    for each_plist_path in full_list_of_plists:
        report = plugin_manager.handle_plist(each_plist_path)
        if report:
            if dump_dir is None:
                print(report)
                total_plists_processed += 1
            else:
                filename = re.sub('/', '-', each_plist_path)
                try:
                    full_path = os.path.join(dump_dir, "{}.txt".format(filename[:max_filename_len]))
                    f = open(full_path, 'w')
                    f.write(report)
                    f.close()
                    total_plists_processed += 1
                except OSError:
                    msg = "Could not write file {}".format(filename[:max_filename_len])
                    logging.error(msg)
                    print(msg)

    print('=' * 80)
    if total_plists_processed == len(full_list_of_plists):
        end_message = 'Processed {}/{} plists.'.format(total_plists_processed, len(full_list_of_plists))
    else:
        end_message = 'Processed {}/{} plists. Rerun with --all for more results.'.format(total_plists_processed, len(full_list_of_plists))

    print(end_message)
    logging.info('Processed {}/{} plists.'.format(total_plists_processed, len(full_list_of_plists)))




if __name__ == '__main__':

    # Enable Logging
    logging.basicConfig(handlers=[logging.FileHandler('log.txt', 'w', 'utf-8')],
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        level=logging.DEBUG)

    logging.info("Program started")

    # Parse all the arguments passed to the program
    parser = argparse.ArgumentParser(description='Locate and parse plists')
    parser.add_argument('path', type=str,
                        help='path to search for plists - can be a mounted disk image')
    parser.add_argument('-a', '--all', action="store_true",
                        help='display report for all plists, even if no plugin is available')
    parser.add_argument('-r', '--recursive', action="store_true",
                        help='examine all subfolders in supplied path')
    parser.add_argument('-f', '--filter', action="store_true",
                        help='run in filter mode; full raw contents of plists will be displayed, but only for those with a plugin')
    parser.add_argument('-l', '--list', action="store_true",
                        help='list plists, do not process')
    parser.add_argument('-b', '--bytes', type=str, choices=['raw', 'hide', 'strings'],
                        help='choose how to display non-printable data')
    parser.add_argument('-d', '--dumpdir', type=str,
                        help='choose output directory for reports rather than printing')
    parser.add_argument('-i', '--include', type=str, nargs='+',
                        help='include files only with specified extensions (takes precedence over exclude)')
    parser.add_argument('-x', '--exclude', type=str, nargs='+',
                        help='exclude files with specified extensions')

    args = parser.parse_args()

    if not os.path.exists(args.path):
        logging.error("Specified path does not exist.")
        print('Path %s does not exist' % args.path)
        sys.exit(-1)

    # If bytes mode is not specified, default to hiding non-printable bytes
    if args.bytes is None:
        args.bytes = "hide"

    # Log all configuration settings
    logging.info("Configuration options:")
    logging.info("\tTarget path: {}".format(args.path))
    logging.info("\tShowing all plists: {}".format(args.all))
    logging.info("\tRecursive directory listing: {}".format(args.recursive))
    logging.info("\tFilter mode: {}".format(args.filter))
    logging.info("\tList mode: {}".format(args.list))
    logging.info("\tBytes display mode: {}".format(args.bytes))
    logging.info("\tExcluding files with the following extensions: {}".format(args.exclude))
    logging.info("\tIncluding files with the following extensions: {}".format(args.include))

    # Generate a full list of all possible plists in path
    full_list_of_plists = get_list_of_plists(path=args.path,
                                             recursive_mode=args.recursive,
                                             include_extensions=args.include,
                                             exclude_extensions=args.exclude)

    # Initialise plist plugin manager
    plugin_manager = plugin_manager.PluginManager(display_unknown=args.all,
                                                 filter_mode=args.filter,
                                                 handle_bytes=args.bytes)

    # Do something with the list of plists
    if args.list:
        # if just listing
        print_list_of_plists(full_list_of_plists, filter_mode=args.filter, plugin_manager=plugin_manager)
    else:
        analyse_list_of_plists(full_list_of_plists, plugin_manager=plugin_manager, dump_dir=args.dumpdir)

    logging.info('Program complete.')
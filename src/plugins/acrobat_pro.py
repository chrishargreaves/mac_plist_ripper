from core import base_plugin

__author__ = 'Chris Hargreaves'


class AcrobatProParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Adobe Acrobat Pro"

        super(AcrobatProParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.adobe\.Acrobat\.Pro\.plist"

        #self.field_list["RecentFiles"] = "RecentFiles"
        self.field_list["RecentFiles"] = "DIText"
        #self.field_list["DocParents"] = "DocParents"
        #self.field_list["DocFiles"] = "DocFiles"
        #self.field_list["DocTitle"] = "DocTitle"
        self.field_list["Identity"] = "Identity"
        self.field_list["NSNavLastRootDirectory"] = "NSNavLastRootDirectory"







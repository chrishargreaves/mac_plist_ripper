from core import base_plugin

__author__ = 'Chris Hargreaves'


class AirportUtilityParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Airport Utility"

        super(AirportUtilityParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.airport\.airportutility\.plist"

        self.field_list["configuredAirPortIDs"] = "configuredAirPortIDs"







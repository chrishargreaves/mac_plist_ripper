from core import base_plugin

__author__ = 'Chris Hargreaves'


class AppStoreParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "App Store"

        super(AppStoreParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.storeagent\.plist"

        self.field_list["AppleID"] = "AppleID"
        self.field_list["LastAuthTime"] = "LastAuthTime"






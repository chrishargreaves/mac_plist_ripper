from core import base_plugin

__author__ = 'Chris Hargreaves'


class AppStoreUpdatesParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "App Store Updates"

        super(AppStoreUpdatesParser, self).__init__()

        self.matching_criteria['path'] = ".*Application Support/App Store/updatejournal\.plist"

        self.field_list["Recently updated app name"] = "title"







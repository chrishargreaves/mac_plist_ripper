from core import base_plugin

__author__ = 'Chris Hargreaves'


class BywordParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Byword"

        super(BywordParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.metaclassy\.byword\.LSSharedFileList\.plist"

        self.field_list["Name"] = "Name"







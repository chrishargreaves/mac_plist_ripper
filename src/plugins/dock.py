from core import base_plugin

__author__ = 'Chris Hargreaves'


class DockParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Dock"

        super(DockParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.dock\.plist"

        self.field_list["Dock item label"] = "file-label"







from core import base_plugin

__author__ = 'Chris Hargreaves'


class FacetimeParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Facetime"

        super(FacetimeParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.FaceTime\.plist"

        self.field_list["FavoritesList"] = "FavoritesList"







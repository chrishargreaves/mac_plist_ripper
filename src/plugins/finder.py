from core import base_plugin

__author__ = 'Chris Hargreaves'


class FinderParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Finder"

        super(FinderParser, self).__init__()

        self.matching_criteria['path'] = "/Users/.+?/Library/Preferences/.*com\.apple\.finder\.plist"

        self.field_list["Recently Accessed Folders"] = "FXRecentFolders"
        self.field_list["Connect to Last URL"] = "FXConnectToLastURL"
        self.field_list["Desktop Volume Positions"] = "FXDesktopVolumePositions"






from core import base_plugin

__author__ = 'Chris Hargreaves'


class GamedParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Game Center"

        super(GamedParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.gamed\.plist"

        self.field_list["natTypeCache"] = "natTypeCache"
        self.field_list["username"] = "username"









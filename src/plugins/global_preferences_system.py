from core import base_plugin

__author__ = 'Chris Hargreaves'


class GlobalPreferencesParserSystem(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "GlobalPreferences(system)"

        super(GlobalPreferencesParserSystem, self).__init__()

        self.matching_criteria['path'] = "^/Library/Preferences/.GlobalPreferences.plist"

        self.field_list['TimeZoneName'] = 'TimeZoneName'
        self.field_list['Latitude'] = 'Latitude'
        self.field_list['Longitude'] = 'Longitude'
        self.field_list['Name'] = 'Name'

        self.field_list['AppleLocale'] = 'AppleLocale'

from core import base_plugin

__author__ = 'Chris Hargreaves'


class GlobalPreferencesParserUser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "GlobalPreferences(user)"

        super(GlobalPreferencesParserUser, self).__init__()

        #self.matching_criteria['path'] = ".*\.GlobalPreferences.*"
        self.matching_criteria['path'] = "^/Users/[^/]+?/Library/Preferences/.GlobalPreferences.plist"

        self.field_list['Locale'] = 'AppleLocale'
        self.field_list['Recent items'] = 'NSNavRecentPlaces'
        self.field_list['User Dictionary Replacement Items'] = 'NSUserDictionaryReplacementItems'
        self.field_list['Web Search Provder'] = 'NSWebServicesProviderWebSearch'


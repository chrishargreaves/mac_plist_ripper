from core import base_plugin


class iChatJabberParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "iChat Jabber Details"

        super(iChatJabberParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.iChat\.Jabber\.plist"

        self.field_list["Username"] = "LoginAs"




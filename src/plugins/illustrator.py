from core import base_plugin


class IllustratorParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Abobe Illustrator"

        super(IllustratorParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.adobe\.illustrator\.plist"

        self.field_list["NSNavLastRootDirectory"] = "NSNavLastRootDirectory"







from core import base_plugin

__author__ = 'Chris Hargreaves'


class InDesignParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Abobe Illustrator"

        super(InDesignParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.adobe\.InDesign\.plist"

        self.field_list["NSNavLastCurrentDirectory"] = "NSNavLastCurrentDirectory"
        self.field_list["NSNavLastRootDirectory"] = "NSNavLastRootDirectory"


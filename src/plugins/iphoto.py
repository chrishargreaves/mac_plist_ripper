from core import base_plugin

__author__ = 'Chris Hargreaves'


class iPhotoParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "iPhoto Settings"

        super(iPhotoParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.iPhoto\.plist"

        self.field_list["LibraryPath"] = "LibraryPath"
        self.field_list["Database Name"] = "DPAPService.databaseName"





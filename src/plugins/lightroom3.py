from core import base_plugin

__author__ = 'Chris Hargreaves'


class Lightroom3Parser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Lightroom 3"

        super(Lightroom3Parser, self).__init__()

        self.matching_criteria['path'] = ".*com\.adobe\.Lightroom3.plist"

        self.field_list["AgImportDialog_destinationFolderPath"] = "AgImportDialog_destinationFolderPath"
        self.field_list["AgImport_backupDownloadFolder"] = "AgImport_backupDownloadFolder"
        self.field_list["AgImport_importDestinationFolderPath"] = "AgImport_importDestinationFolderPath"
        self.field_list["AgImport_lastImportPath"] = "AgImport_lastImportPath"
        self.field_list["AgMRUPopupList_AgImportDialog_destinationFolderPath"] = "AgMRUPopupList_AgImportDialog_destinationFolderPath"
        self.field_list["AgMRUPopupList_importBackupDownloadFolder"] = "AgMRUPopupList_importBackupDownloadFolder"
        self.field_list["libraryToLoad20"] = "libraryToLoad20"
        self.field_list["recentLibraries20"] = "recentLibraries20"
        self.field_list["recentLibraries20_missing"] = "recentLibraries20_missing"








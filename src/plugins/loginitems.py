from core import base_plugin

__author__ = 'Chris Hargreaves'

class LoginItemsParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "LoginItems"

        super(LoginItemsParser, self).__init__()

        self.matching_criteria['path'] = "/Users/[^/]+?/Library/Preferences/com.apple.loginitems.plist"

        self.field_list['Name'] = 'Name'



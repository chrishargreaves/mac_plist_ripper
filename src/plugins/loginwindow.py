from core import base_plugin

__author__ = 'Chris Hargreaves'

class LoginWindowParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "LoginWindow"

        super(LoginWindowParser, self).__init__()

        self.matching_criteria['path'] = "/Users/[^/]+?/Library/Preferences/com.apple.loginwindow.plist"

        self.field_list['lastUser'] = 'lastUser'
        self.field_list['lastUserName'] = 'lastUserName'
        self.field_list['RetriesUntilHint'] = 'RetriesUntilHint'
        self.field_list['GuestEnabled'] = 'RetriesUntilHint'



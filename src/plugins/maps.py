from core import base_plugin


class MapsParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Maps"

        super(MapsParser, self).__init__()

        self.matching_criteria['path'] = ".*Library/Containers/com\.apple\.Maps/Data/Library/Preferences/com\.apple\.Maps\.plist"

        self.field_list["LastClosedWindowViewport"] = "LastClosedWindowViewport"
        self.field_list["singleLineAddress"] = "singleLineAddress"






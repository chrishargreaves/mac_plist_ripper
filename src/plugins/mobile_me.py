from core import base_plugin


class MobileMeSettingsParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Mobile Me Settings"

        super(MobileMeSettingsParser, self).__init__()

        self.matching_criteria['path'] = ".*MobileMeAccounts\.plist"

        self.field_list["AccountID"] = "AccountID"
        self.field_list["DisplayName"] = "DisplayName"
        self.field_list["firstName"] = "firstName"
        self.field_list["lastName"] = "lastName"







from core import base_plugin


class NetAuthAgentParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "NetAuthAgent"

        super(NetAuthAgentParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.NetAuthAgent\.plist"

        self.field_list["PreviousAppleIDs"] = "PreviousAppleIDs"
        self.field_list["PreviouslySelectedShares"] = "PreviouslySelectedShares"






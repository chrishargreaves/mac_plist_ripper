from core import base_plugin


class PartialDownloadParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Partial Downloads"

        super(PartialDownloadParser, self).__init__()

        self.matching_criteria['path'] = ".*\.download/Info\.plist"

        self.field_list["DownloadEntryPath"] = "DownloadEntryPath"
        self.field_list["DownloadEntryProgressBytesSoFar"] = "DownloadEntryProgressBytesSoFar"
        self.field_list["DownloadEntryProgressTotalToLoad"] = "DownloadEntryProgressTotalToLoad"
        self.field_list["DownloadEntryURL"] = "DownloadEntryURL"







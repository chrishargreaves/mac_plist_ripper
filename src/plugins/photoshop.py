from core import base_plugin

__author__ = 'Chris Hargreaves'


class PhotoshopParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Abobe Photoshop"

        super(PhotoshopParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.adobe\.Photoshop\.plist"

        self.field_list["NSNavLastRootDirectory"] = "NSNavLastRootDirectory"







from core import base_plugin


class PrintingPrefsParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Printing Preferences"

        super(PrintingPrefsParser, self).__init__()

        self.matching_criteria['path'] = ".*org\.cups\.PrintingPrefs\.plist"

        self.field_list["LastUsedPrinters"] = "LastUsedPrinters"







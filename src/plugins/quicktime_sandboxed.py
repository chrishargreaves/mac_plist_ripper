from core import base_plugin

__author__ = 'Chris Hargreaves'


class QuicktimeParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Quicktime (Sandboxed)"

        super(QuicktimeParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.QuickTimePlayerX\.SandboxedPersistentURLs\.LSSharedFileList\.plist"

        self.field_list["Name"] = "Name"







from core import base_plugin

__author__ = 'Chris Hargreaves'


class SafariBookmarkParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Safari Bookmarks"

        super(SafariBookmarkParser, self).__init__()

        self.matching_criteria['path'] = "/Library/Safari/Bookmarks.plist"

        self.field_list["Bookmarked URL"] = "URLString"
        #self.field_list["title"] = "title"


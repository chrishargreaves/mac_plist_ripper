from core import base_plugin

__author__ = 'Chris Hargreaves'


class SafariSettingsParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Safari Settings"

        super(SafariSettingsParser, self).__init__()

        self.matching_criteria['path'] = "/Users/.+?/Library/Preferences/.*com\.apple\.Safari\.plist"

        self.field_list["DownloadsPath"] = "DownloadsPath"
        self.field_list["HomePage"] = "HomePage"
        self.field_list["NSNavLastRootDirectory"] = "NSNavLastRootDirectory"
        self.field_list["Recent searches"] = "RecentSearchStrings"




from core import base_plugin

__author__ = 'Chris Hargreaves'


class SafariTopSitesParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Safari Bookmarks"

        super(SafariTopSitesParser, self).__init__()

        self.matching_criteria['path'] = "/Library/Safari/TopSites.plist"

        self.field_list["Top Site URL"] = "TopSiteURLString"
        #self.field_list["title"] = "TopSiteTitle"


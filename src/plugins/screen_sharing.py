from core import base_plugin

__author__ = 'Chris Hargreaves'


class ScreenSharingParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "ScreenSharing"

        super(ScreenSharingParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.ScreenSharing\.LSSharedFileList\.plist"

        self.field_list["Name"] = "Name"









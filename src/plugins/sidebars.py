from core import base_plugin

__author__ = 'Chris Hargreaves'


class SidebarListsParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Sidebar"

        super(SidebarListsParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.sidebarlists\.plist"

        self.field_list["Volumes List Entry"] = "Name"







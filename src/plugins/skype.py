from core import base_plugin

__author__ = 'Chris Hargreaves'


class SkypeParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Skype Settings"

        super(SkypeParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.skype\.skype\.plist"

        self.field_list["SKRecentlyCalledPSTNNumbers"] = "SKRecentlyCalledPSTNNumbers"







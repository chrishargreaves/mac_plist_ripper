from core import base_plugin

__author__ = 'Chris Hargreaves'

class SoftwareUpdateParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "SoftwareUpdates"

        super(SoftwareUpdateParser, self).__init__()

        self.matching_criteria['path'] = "/Users/[^/]+?/Library/Preferences/com.apple.SoftwareUpdate.plist"

        self.field_list['RecommendedUpdates'] = 'RecommendedUpdates'
        self.field_list['LastUpdatesAvailable'] = 'LastUpdatesAvailable'
        self.field_list['LastAttemptSystemVersion'] = 'LastAttemptSystemVersion'
        self.field_list['LastSuccessfulDate'] = 'LastSuccessfulDate'
        self.field_list['LastAttemptDate'] = 'LastAttemptDate'


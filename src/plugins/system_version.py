from core import base_plugin

__author__ = 'Chris Hargreaves'

class SystemVersionParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "SystemVersion"

        super(SystemVersionParser, self).__init__()

        self.matching_criteria['path'] = "/System/Library/CoreServices/SystemVersion.plist"

        self.field_list['ProductName'] = 'ProductName'
        self.field_list['ProductVersion'] = 'ProductVersion'
        self.field_list['ProductUserVisibleVersion'] = 'ProductUserVisibleVersion'
        self.field_list['ProductBuildVersion'] = 'ProductBuildVersion'

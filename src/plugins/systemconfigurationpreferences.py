from core import base_plugin

__author__ = 'Chris Hargreaves'


class SystemConfigurationPreferencesParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "System Configuration Preferences"

        super(SystemConfigurationPreferencesParser, self).__init__()

        #self.matching_criteria['path'] = ".*\.GlobalPreferences.*"
        self.matching_criteria['path'] = "/Library/Preferences/SystemConfiguration/preferences.plist"

        self.field_list['Model'] = 'Model'
        self.field_list['LocalHostName'] = 'LocalHostName'
        self.field_list['ComputerName'] = 'ComputerName'
        self.field_list['BackToMyMac'] = 'BackToMyMac'



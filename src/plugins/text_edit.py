from core import base_plugin

__author__ = 'Chris Hargreaves'


class TextEditParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "TextEdit"

        super(TextEditParser, self).__init__()

        self.matching_criteria['path'] = ".*com\.apple\.TextEdit\.LSSharedFileList\.plist"

        self.field_list["Recent Documents"] = "Name"







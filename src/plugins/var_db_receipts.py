from core import base_plugin

__author__ = 'Chris Hargreaves'

class VarDbReceiptParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "Var Db Receipt"

        super(VarDbReceiptParser, self).__init__()

        self.matching_criteria['path'] = "/private/var/db/receipts/.+\.plist"

        self.field_list['PackageFileName'] = 'PackageFileName'
        self.field_list['PackageVersion'] = 'PackageVersion'
        self.field_list['InstallDate'] = 'InstallDate'
        self.field_list['PackageIdentifier'] = 'PackageIdentifier'

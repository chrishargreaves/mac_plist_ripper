from core import base_plugin

__author__ = 'Chris Hargreaves'


class VlcParser(base_plugin.PlistProcessor):

    def __init__(self):
        """Builds a parser, fails if can't get the information"""

        self.title = "VLC"

        super(VlcParser, self).__init__()

        self.matching_criteria['path'] = ".*org\.videolan\.vlc\.LSSharedFileList\.plist"

        self.field_list["Name"] = "Name"






